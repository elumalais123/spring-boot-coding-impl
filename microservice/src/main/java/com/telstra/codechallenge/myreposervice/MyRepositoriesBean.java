package com.telstra.codechallenge.myreposervice;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MyRepositoriesBean {

	  private List<Items> items;
	  public MyRepositoriesBean() {
		// TODO Auto-generated constructor stub
	}


	@JsonIgnoreProperties(ignoreUnknown = true)
	  @Data
	  static class Items {

	    
	    private String html_url;
		private Long watchers_count;
	    private String language;
		private String description;
	    private String name;
	  }

}
