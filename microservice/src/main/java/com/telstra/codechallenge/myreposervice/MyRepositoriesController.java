package com.telstra.codechallenge.myreposervice;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyRepositoriesController {

	  private MyRepositoriesService myRepositoriesService;

	  public MyRepositoriesController(
			  MyRepositoriesService myRepositoriesService) {
	    this.myRepositoriesService = myRepositoriesService;
	  }
	  

	  @RequestMapping(path = "/myrepositoriesdetails", method = RequestMethod.GET)
	  public MyRepositoriesBean myrepositoriesdetails() {
	    return myRepositoriesService.getMyRepositoriesDetails();
	  }
	  
	  @RequestMapping(path = "/myrepodetails", method = RequestMethod.GET)
	  public MyRepoBean myrepodetails() {
	    return myRepositoriesService.getMyRepoDetails();
	  }
	  	  
}
