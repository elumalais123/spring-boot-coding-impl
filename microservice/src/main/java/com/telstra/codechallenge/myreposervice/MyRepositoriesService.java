package com.telstra.codechallenge.myreposervice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class MyRepositoriesService {

	  @Value("${repo.base.url2}")
	  private String repoBaseUrl;
	  
	  @Value("${repo.base.url}")
	  private String repoBaseUr;
	  private RestTemplate restTemplate;

	  public MyRepositoriesService(RestTemplate restTemplate) {
	    this.restTemplate = restTemplate;
	  }


	  /**
	   * Returns an  spring boot MyRepositoriesBean. Taken from https://developer.github.com/v3/search/#search-repositories
	   *
	   * @return - a MyRepositoriesBean
	   */
	  public MyRepositoriesBean getMyRepositoriesDetails() {
	    return restTemplate.getForObject(repoBaseUrl + "", MyRepositoriesBean.class);
	  }
	  
	  public MyRepoBean getMyRepoDetails() {
		    return restTemplate.getForObject(repoBaseUr + "", MyRepoBean.class);
		  }
}
