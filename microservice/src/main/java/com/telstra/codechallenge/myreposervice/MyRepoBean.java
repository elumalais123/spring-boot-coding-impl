package com.telstra.codechallenge.myreposervice;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MyRepoBean {
	
	  private List<Items> items = new ArrayList<Items>();

	  public MyRepoBean() {
		// TODO Auto-generated constructor stub
	}


	@JsonIgnoreProperties(ignoreUnknown = true)
	  @Data
	  static class Items {


	    
	    
	    private Long id;
	    private String login;
	    private String html_url;
	  }

}

